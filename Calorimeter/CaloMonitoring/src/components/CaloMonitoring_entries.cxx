#include "../CaloBaselineMonAlg.h"
#include "../TileCalCellMonAlg.h"
#include "../LArCellMonAlg.h"
#include "../LArClusterCellMonAlg.h"

DECLARE_COMPONENT(CaloBaselineMonAlg)
DECLARE_COMPONENT(TileCalCellMonAlg)
DECLARE_COMPONENT(LArCellMonAlg)
DECLARE_COMPONENT(LArClusterCellMonAlg)
