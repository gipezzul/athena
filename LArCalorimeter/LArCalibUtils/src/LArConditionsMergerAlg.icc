//Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

template<class T, class T1>
StatusCode LArConditionsMergerAlg<T,T1>::initialize() {

  ATH_CHECK(m_readKeys.initialize(!m_readKeys.empty()));
  //ATH_CHECK(m_detStoreKeys.initialize(!m_detStoreKeys.empty()));

 return StatusCode::SUCCESS;
} 


template<class T, class T1>
StatusCode LArConditionsMergerAlg<T,T1>::execute() {
	   
  if (m_readKeys.empty()) {
     if(m_detStoreKeys.empty()) return StatusCode::FAILURE;

     std::unique_ptr<T1> out=std::make_unique<T1>();
     ATH_CHECK(out->setGroupingType(m_groupingType,msg()));
     ATH_CHECK(out->initialize());
  
     for (auto key : m_detStoreKeys){
         //SG::ReadHandle<T1> calContainer(key);
         const T1* t1Container = nullptr;
         StatusCode sc = detStore()->retrieve(t1Container,key);
         // Check that this container is present
         if (sc.isFailure() || !t1Container) {
            ATH_MSG_WARNING("No container found in storegate  "<< key);
         } else {
            const T1 obj=*t1Container;
            bool stat=out->merge(obj);
            if (stat) {
               ATH_MSG_ERROR("Channels were overwritten while merging " << key);
            }
         }
     }

     ATH_CHECK(detStore()->record(std::move(out),m_writeKey));

  } else {   
     std::unique_ptr<T> out=std::make_unique<T>();
     ATH_CHECK(out->setGroupingType(m_groupingType,msg()));
     ATH_CHECK(out->initialize());
  
     for (auto& key : m_readKeys) {
       SG::ReadCondHandle<T> handle{key};
       const T* obj=*handle;
       bool stat=out->merge(*obj);
       if (stat) {
         ATH_MSG_ERROR("Channels were overwritten while merging " << key);
       }
     }
  
     ATH_CHECK(detStore()->record(std::move(out),m_writeKey));
  }
  
  return StatusCode::SUCCESS;
} 
