 #include "TrkVertexWeightCalculators/SumPtVertexWeightCalculator.h"
 #include "TrkVertexWeightCalculators/JetRestrictedSumPtVertexWeightCalculator.h"
 #include "TrkVertexWeightCalculators/TrueVertexDistanceWeightCalculator.h"

 
 using namespace Trk;
 
 DECLARE_COMPONENT( SumPtVertexWeightCalculator )
 DECLARE_COMPONENT( JetRestrictedSumPtVertexWeightCalculator )
 DECLARE_COMPONENT( TrueVertexDistanceWeightCalculator )

