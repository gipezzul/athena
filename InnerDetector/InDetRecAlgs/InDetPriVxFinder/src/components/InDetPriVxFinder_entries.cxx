#include "InDetPriVxFinder/InDetPriVxFinder.h"
#include "InDetPriVxFinder/InDetPriVxDummyFinder.h"
#include "InDetPriVxFinder/InDetPriVxResorter.h"

DECLARE_COMPONENT( InDet::InDetPriVxFinder )
DECLARE_COMPONENT( InDet::InDetPriVxDummyFinder )
DECLARE_COMPONENT( InDet::InDetPriVxResorter )

