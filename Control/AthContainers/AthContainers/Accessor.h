// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/Accessor.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2024
 * @brief Forwarding header for compatibility with r25.
 */


#ifndef ATHCONTAINERS_ACCESSOR_H
#define ATHCONTAINERS_ACCESSOR_H


#include "AthContainers/AuxElement.h"


#endif // not ATHCONTAINERS_ACCESSOR_H
