// This file's extension implies that it's C, but it's really -*- C++ -*-.
/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/ConstAccessor.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2024
 * @brief Forwarding header for compatibility with r25.
 */


#ifndef ATHCONTAINERS_CONSTACCESSOR_H
#define ATHCONTAINERS_CONSTACCESSOR_H


#include "AthContainers/AuxElement.h"


#endif // not ATHCONTAINERS_CONSTACCESSOR_H
