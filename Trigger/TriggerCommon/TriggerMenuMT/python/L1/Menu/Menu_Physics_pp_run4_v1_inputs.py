# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#
# ALL CHANGES TO THE RUN 4 MENU SHOULD BE MADE TO RELEASE 25 OR GREATER
#

import TriggerMenuMT.L1.Menu.Menu_Physics_pp_run3_v1_inputs as Run3

def defineInputsMenu():
    # reuse based on run3
    Run3.defineInputsMenu()
